# Welcome to Oscar and Benjamins Farmbot Industrial application Project. 

Within this gitlab you will be able to find usefull links that we used for our research here in the readme and our code + poster withing the folders
The goal of the project was to try and replace the integrated MQTT of the farmbot with solution that gave the user more personal control as needed for industrial uses with OPC UA. 

## Farmbot Documentation:
- https://developer.farm.bot/v14/Documentation/firmware.html
- https://software.farm.bot/v14/FarmBot-Software/intro


## OPC UA Documentation: 
- https://www.youtube.com/watch?v=mEbPHflLNyc&list=PLWw98q-Xe7iGf-c4b6zF0bnJA9avEN_mF
- https://www.opc-router.com/what-is-opc-ua/

