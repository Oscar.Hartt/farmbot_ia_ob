from opcua import Server
from random import randint
import time
import serial

server = Server()
url = 'opc.tcp://localhost:4840' #Not local host but RPI IP address
server.set_endpoint(url)

addspace = server.register_namespace('OPC_TEST')

node = server.get_objects_node()

param = node.add_object(addspace, "Parameters")

var = param.add_variable(addspace, "F or G Code", 10)
var.set_writable()

server.start()
print("Server started at", url)

ser = serial.Serial('/dev/ttyACM0', 115200)


#Defining how it encodes the message so the farmbot reads it properly


def command(ser, send_command):
    ser.write(str.encode(send_command))
    sleep(1)


#Now we are trying to loop the command sent from the client as the serial input to the farmbot


while True:
    t = var.get_value()
    print("Last Command {}".format(t))
    time.sleep(10)
    command(ser, "FF22 P2 V1 Q0\r\n")
    command(ser, t)