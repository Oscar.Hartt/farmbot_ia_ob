from opcua import Client


url = 'opc.tcp://localhost:4840'
client = Client(url)

client.connect()
print("client connected")

while True:

    var = client.get_node("ns=2;i=2")
    print("Initial Value {}".format(var.get_value()))
    var.set_value(input("Please enter viable F or G Code command from the FarmBot Library i.e. 'G00 X100 Y200 Z300\\r\\n'\n"))

